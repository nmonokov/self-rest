/**
 * Copyright (c) 2020-2021, Self XDSD Contributors
 * All rights reserved.
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"),
 * to read the Software only. Permission is hereby NOT GRANTED to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software.
 * <p>
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.selfxdsd.rest;

import com.selfxdsd.api.Self;
import com.selfxdsd.api.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.annotation.RequestScope;

import javax.servlet.http.HttpServletRequest;

/**
 * Produces the User. Authenticates the User in Self using the
 * api token provider in the HTTP Request.
 * @author Mihai Andronache (amihaiemil@gmail.com)
 * @version $Id$
 * @since 0.0.1
 * @todo #7:60min Implement the ForbiddenAccess runtime exception,
 *  to be thrown from method authenticate(...) when the bearer token is
 *  missing or not correct. Then, catch this method in an ExceptionHandler
 *  and return the FORBIDDEN http status.
 */
@Configuration
public class UserProducer {

    /**
     * Self.
     */
    private final Self self;

    /**
     * Ctor.
     * @param self Self.
     */
    @Autowired
    public UserProducer(final Self self){
        this.self = self;
    }

    /**
     * Authenticate and return the User.
     * @param request HTTP Request.
     * @return Login.
     */
    @Bean
    @RequestScope
    public User authenticate(final HttpServletRequest request) {
        final String token = this.getAccessToken(request);
        if(token == null) {
            throw new IllegalStateException("Access token is missing.");
        } else {
            return this.self.authenticate(token);
        }
    }

    /**
     * Get the access token from the Request.
     * @param request HTTP Request.
     * @return String token or null if it's missing.
     */
    private String getAccessToken(final HttpServletRequest request) {
        final String token;
        final String authorization = request.getHeader("Authorization");
        if(authorization == null || authorization.isEmpty()) {
            token = null;
        } else {
            final String[] parts = authorization.trim().split(" ");
            if (parts.length != 2) {
                token = null;
            } else {
                if (parts[0].equalsIgnoreCase("Bearer")
                    || parts[0].equalsIgnoreCase("Token")
                ) {
                    token = parts[1];
                } else {
                    token = null;
                }
            }
        }
        return token;
    }

}
